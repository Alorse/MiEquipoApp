function aos_teams(){
	var teams = [];
	var _self = this;
	var clean_teams = {};
	this.clash = [];
	/**
     * [_init Constructor por defecto]
     * @return {[type]} [description]
     */
	this._init = function(){
		console.log('Constructor teams Class');
	};
	/**
	 * [create Crea un nuevo equipo de futbol, solo está como guía]
	 * @return {[type]} [description]
	 */
	this.create = function(){
		var GameScore = Parse.Object.extend("teams");
		var gameScore = new GameScore();

		gameScore.set("name","América de Cali");
		gameScore.set("slug", "america-de-cali");
		gameScore.set("stars", 3);
		gameScore.set("colors", ["rojo", "blanco"]);
		gameScore.set("city", "Cali");
		gameScore.set("stadium", "Olímpico Pascual Guerrero");

		gameScore.save(null, {
		  success: function(gameScore) {
		    // Execute any logic that should take place after the object is saved.
		    alert('New object created with objectId: ' + gameScore.id);
		  },
		  error: function(gameScore, error) {
		    // Execute any logic that should take place if the save fails.
		    // error is a Parse.Error with an error code and message.
		    alert('Failed to create new object, with error code: ' + error.message);
		  }
		});
	};
	/**
	 * [get Consume los equipos de futbol desde el backend]
	 * @param  {[type]} $scope [Scope de AngularJS]
	 * @return {[type]}        [description]
	 */
	this.get = function($scope){
		teams = [];
		var GameScore = Parse.Object.extend("teams");
		var query = new Parse.Query(GameScore);
		var currentUser = Parse.User.current();
		query.find({
			success: function(myObject) {
				myObject.forEach(function(a){
					var item = {
						id: a.id,
						city: a.attributes.city,
						colors: a.attributes.colors.toString(),
						name: a.attributes.name,
						slug: a.attributes.slug,
						stadium: a.attributes.stadium,
						stars: a.attributes.stars
					};
					teams.push(item);
					clean_teams[a.attributes.slug] = item;
				});
				$scope.teams = teams;
				$('#fpc').val(0);
				if(currentUser.attributes.team){
					$scope.update(currentUser.attributes.team);
					$('#fpc').val();
					$('#myteam').show();
					setTimeout(function(){
						$('#fpc option[value=' + currentUser.attributes.team + ']').attr('selected','selected');	
					}, 10);
				} else 
					$scope.myteam = {};
			},
			error: function(myObject, error) {
				console.log(myObject);
				console.log(error);
			}
		});

		$scope.update = function(team) {
			$scope.myteam = {
				city: clean_teams[team].city,
				name: clean_teams[team].name,
				colors: clean_teams[team].colors,
				img: 'https://rippleapi.herokuapp.com/xhr_proxy?tinyhippos_apikey=ABC&tinyhippos_rurl=http://www.colombia.com/futbol/equipos/' + team + '/images/escudo.jpg',
				stadium: clean_teams[team].stadium,
				stars: clean_teams[team].stars
			};
			$('#myteam').show();
			currentUser.set('team', team);
			_self.nextGame(currentUser.attributes.team);
			return currentUser.save();
		}
	};
	/**
	 * [nextGame Obtiene los posibles encuentros del equipo seleccionado]
	 * @param  {[type]} team [Slug del equipo de futbol]
	 * @return {[type]}      [description]
	 */
	this.nextGame = function(team){
		_self.nextClash(team, 1, function(obj){
			_self.clash[1] = obj;
		});
		_self.nextClash(team, 2, function(obj){
			_self.clash[2] = obj;
		});
	};
	/**
	 * [nextClash Consume información desde backend de un partido]
	 * @param  {[type]}   team     [Slug del equipo de futbol]
	 * @param  {[type]}   pos      [1 para consumir equipo1 del encuentro, 2 para consumir equipo2]
	 * @param  {Function} callback [Función que se ejecutará luego de consumido el servicio]
	 * @return {[type]}            [description]
	 */
	this.nextClash = function(team, pos, callback){
		var GameScore = Parse.Object.extend("clashes");
		var query = new Parse.Query(GameScore);
		var d = new Date();
		var time = (2 * 3600 * 1000); //2 horas menos, por si estan en el
		var todaysDate = new Date(d.getTime() - time);
		query.greaterThanOrEqualTo( "time", todaysDate );
		query.equalTo("team" + pos, team);
		query.ascending("time");
		query.limit(1);
		query.find({
			success: function(myObject) {
				callback(myObject);
			},
			error: function(myObject, error) {
				console.log(myObject);
				console.log(error);
			}
		});
	};
	/**
	 * [getNextGame Encuentra el próximo partido de un equipo]
	 * @param  {[type]} $scope [Scope de AngularJS]
	 * @return {[type]}        [description]
	 */
	this.getNextGame = function($scope){
		var nextClash = [];
		if(_self.clash[1].length != 0 && _self.clash[2].length == 0){
			nextClash = _self.clash[1];
		} else if(_self.clash[1].length == 0 && _self.clash[2].length != 0){
			nextClash = _self.clash[2];
		} else if(_self.clash[1].length != 0 && _self.clash[2].length != 0){
			if(_self.clash[1][0].attributes.time < _self.clash[2][0].attributes.time ){
				nextClash = _self.clash[1];
			} else {
				nextClash = _self.clash[2];
			}
		}
		_self.loadVS($scope, nextClash);
	};
	/**
	 * [loadVS Carga la información de un partido]
	 * @param  {[type]} $scope    [Scope de AngularJS]
	 * @param  {[type]} nextClash [description]
	 * @return {[type]}           [description]
	 */
	this.loadVS = function($scope, nextClash){
		if(nextClash.length != 0){
			$('#nextgame section').show();
			var t1 = nextClash[0].attributes.team1;
			var t2 = nextClash[0].attributes.team2;
			var d = new Date(nextClash[0].attributes.time)
			$scope.clash = {
				team1: clean_teams[t1].name,
				team1img: 'https://rippleapi.herokuapp.com/xhr_proxy?tinyhippos_apikey=ABC&tinyhippos_rurl=http://www.colombia.com/futbol/equipos/' + t1 + '/images/escudo.jpg',
				team2: clean_teams[t2].name,
				team2img: 'https://rippleapi.herokuapp.com/xhr_proxy?tinyhippos_apikey=ABC&tinyhippos_rurl=http://www.colombia.com/futbol/equipos/' + t2 + '/images/escudo.jpg',
				hour: d.getHours() + ":" + (d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()),
				day: d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear(),
				channel: nextClash[0].attributes.channel
			};
		} else {
			$('#nextgame section').hide();
			setTimeout(function(){
				$('#nextgame h3').text("No hay partidos programados.");
			}, 1);
		}
	};
}