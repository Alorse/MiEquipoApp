function aos_user(){
	var _self = this;
	/**
     * [_init Constructor por defecto]
     * @return {[type]} [description]
     */
	this._init = function(){
		console.log('Constructor User Class');
	};
	/**
	 * [register Registra al usuario en el backend]
	 * @param  {[type]} $scope    	 [Scope de AngularJS]
	 * @param  {[type]} $routeParams [routeParams de AngularJS]
	 * @return {[type]}              [description]
	 */
	this.register = function($scope, $routeParams){
		$('form#signupform').validator().on('submit', function(e) {
			e.preventDefault();
			var username = $("#username").val();
			var mobile = $("#mobile").val();
			var email = $("#email").val();
			var password = $("#password").val();

			var user = new Parse.User();
			user.set("username", username);
			user.set("password", password);
			user.set("email", email);

			// user.set("phone", mobile);

			user.signUp(null, {
				success: function(user) {
					window.location.href = '#/';
				},
				error: function(user, error) {
					console.log(error);
					function alertDismissed(){}
					navigator.notification.alert(
					    error.message,  // message
					    alertDismissed,         // callback
					    'Estimado Usuario',            // title
					    'OK'                  // buttonName
					);
				}
			});
			$("#signup .button").attr("disabled", "disabled");
		});
	};
	/**
	 * [logIn Identifica a un usuario en el sistema]
	 * @param  {[type]} $scope    [Scope de AngularJS]
	 * @return {[type]}   [description]
	 */
	this.logIn = function($scope) {
		var username = $("#login-username").val();
		var password = $("#login-password").val();

		Parse.User.logIn(username, password, {
			success: function(user) {
				$(".error").html('').hide();
				window.location.href = '#/';
			},
			error: function(user, error) {
				console.log(error);
				function alertDismissed(){}
				navigator.notification.alert(
				    error.message,  // message
				    alertDismissed,         // callback
				    'Estimado usuario',            // title
				    'OK'                  // buttonName
				);
			}
		});

		$("#login .button").attr("disabled", "disabled");

		return false;
	};
}