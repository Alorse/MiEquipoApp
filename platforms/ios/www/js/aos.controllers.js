function aos_controllers(){
	var _self = this;

	this._init = function(){
		console.log('Constructor controllers Class');
	};

	this.get = function(ctrl){
		ctrl.config(['$routeProvider', 'uiGmapGoogleMapApiProvider',
			function($routeProvider, uiGmapGoogleMapApiProvider) {
				$routeProvider
				.when('/', {
					templateUrl: function(urlattr){
						var view = '';
						if(Parse.User.current() == null){
							view = 'view/home.html';
						} else {
							view = 'view/user_home.html'
						}
						return view;
					},
					controller: 'HomeController'
				})
				.when('/Register', {
					templateUrl: 'view/user_register.html',
					controller: 'RegisterController'
				})
				.when('/Login', {
					templateUrl: 'view/user_login.html',
					controller: 'LoginController'
				})
				.when('/NextGame', {
					templateUrl: 'view/team_nextgame.html',
					controller: 'NextGameController'
				})
				.when('/Stores', {
					templateUrl: 'view/stores.html',
					controller: 'StoresController'
				})
				.otherwise({
					redirectTo: '/'
				});
				uiGmapGoogleMapApiProvider.configure({
			        //    key: 'your api key',
			        v: '3.17',
			        libraries: 'weather,geometry,visualization'
			    });
			}
		]);

		ctrl.controller('LoginController', function($scope) {
			$('#backbtn').show();
			$('#loginbox form').on('submit', function(e) {
	    		e.preventDefault();
	    		_user.logIn($scope);
			});
		});
 
		ctrl.controller('HomeController', function($scope) {
			user = Parse.User.current();
			if(user){
				$('#openmenu').show();
				$('#backbtn').hide();
				$scope.username = user.attributes.username;
				$scope.email = user.attributes.email;
				_teams.get($scope);
			} else {
				$('#openmenu').hide();
				$('#backbtn').hide();
			}
		});

		ctrl.controller('StoresController', function($scope, uiGmapGoogleMapApi) {
			$('#myNavmenu').offcanvas('hide');
			_map.getMap($scope, uiGmapGoogleMapApi);
		});
		
		ctrl.controller('NextGameController', function($scope) {
			_teams.getNextGame($scope);
			$('#myNavmenu').offcanvas('hide');
		});

		ctrl.controller('RegisterController', function($scope, $routeParams) {
			$('#backbtn').show();
			_user.register($scope, $routeParams);
		});
		
		ctrl.controller('miequipoCtrl', function ($scope) {
			user = Parse.User.current();
			if(user){
				$scope.username = user.attributes.username;
				$scope.email = user.attributes.email;
			}
			$('.loading_fullscreen').hide();
		});
		$('#backbtn').hide();
	};
}