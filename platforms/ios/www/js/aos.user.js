function aos_user(){
	var _self = this;
	this._init = function(){
		console.log('Constructor User Class');
	};
	this.load = function(){
		
	};
	this.register = function($scope, $routeParams){
		$('form#signupform').validator().on('submit', function(e) {
			e.preventDefault();
			var username = $("#username").val();
			var mobile = $("#mobile").val();
			var email = $("#email").val();
			var password = $("#password").val();

			var user = new Parse.User();
			user.set("username", username);
			user.set("password", password);
			user.set("email", email);

			// other fields can be set just like with Parse.Object
			// user.set("phone", mobile);

			user.signUp(null, {
				success: function(user) {
					window.location.href = '#/';
				},
				error: function(user, error) {
					console.log(error);
					function alertDismissed(){}
					navigator.notification.alert(
					    error.message,  // message
					    alertDismissed,         // callback
					    'Estimado Usuario',            // title
					    'OK'                  // buttonName
					);
				}
			});
			$("#signup .button").attr("disabled", "disabled");
		});
	};
	/**
	 * [logIn description]
	 * @param  {[type]} e [description]
	 * @return {[type]}   [description]
	 */
	this.logIn = function($scope) {
		var username = $("#login-username").val();
		var password = $("#login-password").val();

		Parse.User.logIn(username, password, {
			success: function(user) {
				$(".error").html('').hide();
				window.location.href = '#/';
			},
			error: function(user, error) {
				console.log(error);
				function alertDismissed(){}
				navigator.notification.alert(
				    error.message,  // message
				    alertDismissed,         // callback
				    'Estimado usuario',            // title
				    'OK'                  // buttonName
				);
			}
		});

		$("#login .button").attr("disabled", "disabled");

		return false;
	};
}