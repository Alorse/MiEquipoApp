function aos_pointsmap(){
	var _self = this;
	var pos = {};
	var points = [];
	this._init = function(){
		console.log('Constructor pointsmap Class');
	};
	this.getMap = function($scope, uiGmapGoogleMapApi){
		uiGmapGoogleMapApi.then(function(maps) {
			console.log("Google maps api loaded");
		});
		$scope.map = { 
			center: { latitude: _self.pos.coords.latitude, longitude: _self.pos.coords.longitude }, 
			zoom: 12
		};
		$('.angular-google-map-container').height($(document).height()-50);
		_self.getPoints($scope);
	};
	this.onSuccess = function(position) {
		_self.pos = position;
	};
	this.onError = function(error) {
	    console.log('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
	}
	this.getPoints = function($scope){
		_self.points = [];
		$scope.Markers = [];
		var markers = [];
		var GameScore = Parse.Object.extend("Outlets");
		var query = new Parse.Query(GameScore);
		var currentUser = Parse.User.current();
		query.find({
			success: function(myObject) {
				var c = 0;
				myObject.forEach(function(p){
					var item = {
						id: c,
						title: p.attributes.name,
						description: p.attributes.description,
						latitude: p.attributes.point._latitude,
						longitude: p.attributes.point._longitude
					};
					_self.points.push(item);
					markers.push(item);
					c++;
				});
				$scope.Markers = markers;
			},
			error: function(myObject, error) {
				console.log(myObject);
				console.log(error);
			}
		});
	};
	this.paintMarkers = function($scope){

	};
}