function aos_main (){
	var self = this;
    var ISMOBILE = (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile|ipad|android 3|xoom|sch-i800|playbook|tablet|kindle/i.test(navigator.userAgent.toLowerCase()));
	this._init = function(){
		this.bindEvents();
	};
	this.bindEvents = function() {
        if(ISMOBILE){
            document.addEventListener('deviceready', this.onDeviceReady, false);
        }else{
            self.onDeviceReady();
        }
    };
    this.onDeviceReady = function(){
    	self.initialize();
    	self.actions();
    	self.checkConnection();
        if(ISMOBILE){
            setTimeout(function(){
                if (parseFloat(window.device.version) >= 7.0 && device.platform === 'iOS') {
                    $('.navbar').css('padding-top','20px');
                    document.body.style.paddingTop = "20px";
                }
            }, 100);
        }
    };
    this.initialize = function(){
        var mi_equipo = angular.module('mi-equipo', ['parse-angular', 'ngRoute', 'uiGmapgoogle-maps']);
        _controllers.get(mi_equipo);
        angular.bootstrap($('body'), ['mi-equipo']);
		navigator.geolocation.getCurrentPosition(_map.onSuccess, _map.onError);
    };
    this.actions = function(){
    	$(document).on("click", ".closeSession", function() {
			Parse.User.logOut();
			$('#myNavmenu').offcanvas('hide');
			window.location.href = '#/';
		});
		$(document).on("click", "#backbtn", function() {
			window.history.back();
		});
		$(document).on("click", ".miteam", function() {
			$('#myNavmenu').offcanvas('hide');
		});
    };
    this.checkConnection = function() {
        if(ISMOBILE){
            if(navigator.connection.type == Connection.NONE){
            	function alertDismissed(){}
    			navigator.notification.alert(
    			    'Revise la conexión a internet para continuar',  // message
    			    alertDismissed,         // callback
    			    'Estimado Usuario',            // title
    			    'OK'                  // buttonName
    			);
            }
        }
    };
}

Parse.initialize("MGM3aOep8rW0CITIjlAPKn4JgMcLcKjd40bvGbLY", "ANNnLwSsOCQCDON5BtaF5KQNTnge4hAK7yBaP59g");
//Call objects
var _controllers = new aos_controllers(); _controllers._init();
var _map = new aos_pointsmap(); _map._init();
var _teams = new aos_teams(); _teams._init();
var _user = new aos_user(); _user._init();
var main = new aos_main();main._init();